from tp2 import *

# on veut pouvoir creer des boites
def test_box_create():
    b = Box()

# on veut pouvoir mettre des trucs dedans
#def test_box_add():
#    b = Box()
#    b.add("truc1")
#    b.add("truc2")
#    assert "truc1" in b
#    assert "truc2" in b
#    assert "salut" not in b
#
#def test_box_sup():
#    b=Box()
#    b.add("truc1")
#    b.add("truc2")
#    assert "truc1" in b
#    b.sup("truc1")
#    assert "truc1" not in b
#    assert "truc2" in b
#
#def test_box_ouv_ferm():
#    b=Box()
#    assert not b.is_open()
#    b.open()
#    assert b.is_open()
#    b.close()
#    assert not b.is_open()
#
#def test_box_look():
#    b=Box()
#    assert b.action_look()=="la boite est fermee"
#    b.open()
#    b.add("truc1")
#    b.add("truc2")
#    b.action_look()=="la boite contient: truc1, truc2"
#
def test_thing_create():
    t=Thing(3,"truc1")

def test_thing_poid():
    t=Thing(3,"truc1")
    assert t.volume()==3
    assert t.get_name()=="truc1"
    assert t.has_name("truc1")

def test_box_capacity():
    b=Box()
    assert b.capacity()==None
    b.set_capacity(5)
    assert b.capacity()==5

def test_box_reste_capacity():
    b=Box()
    b.set_capacity(10)
    t=Thing(3,"truc1")
    b.add(t)
    assert b.capacity()==7
    t2=Thing(4,"truc2")
    assert b.has_room_for(t2)
    b.add(t2)
    assert b.capacity()==3
    b2=Box()
    assert b2.has_room_for(t)

def test_box_action_add():
    b=Box()
    b.set_capacity(10)
    t1=Thing(7,"truc1")
    t2=Thing(5,"truc2")
    b.open()
    assert b.action_add(t1)
    assert not b.action_add(t2)
    t3=Thing(2,"truc3")
    b.close()
    assert not b.action_add(t3)

def test_box_find():
    b=Box()
    t1=Thing(1,"truc1")
    t2=Thing(1,"truc2")
    b.open()
    b.action_add(t1)
    assert b.find("truc1")==t1
    assert b.find("machin")==None
    b.close
    assert b.find("truc2")==None

